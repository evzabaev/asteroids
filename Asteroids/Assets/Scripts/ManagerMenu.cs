using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerMenu : MonoBehaviour
{
    [SerializeField] private GameObject menuWindow;

    [SerializeField] private GameObject recordWindow;
    [SerializeField] private GameObject textRecordTable;
    [SerializeField] private GameObject prefabTextRecord;


    //private string lengthRecords = "lengthRecords";
    //private string stringScoreRecord = "record";
    //private string stringNameRecord = "Name";

    public struct statisticPlayer
    {
        public string name;
        public int score;
    }

    private void fullingRecordTable()
    {
        int lenRecords = 1;
        
        GameObject _gameObject;
        Text text;

        clearTable();

        if (!PlayerPrefs.HasKey(stateSave.lengthRecords.ToString()))
        {
            _gameObject = Instantiate(prefabTextRecord, textRecordTable.transform);
            text = _gameObject.GetComponent<Text>();
            text.text = "Records not found";
        }
        else
        {
            lenRecords = PlayerPrefs.GetInt(stateSave.lengthRecords.ToString());

            statisticPlayer[] _statisticPlayer = new statisticPlayer[lenRecords];

            for (int i = 0; i < lenRecords; ++i)
            {
                _statisticPlayer[i].name = PlayerPrefs.GetString(stateSave.stringNameRecord.ToString() + i.ToString());
                _statisticPlayer[i].score = PlayerPrefs.GetInt(_statisticPlayer[i].name);
            }

            _statisticPlayer = sortingRecordClass(_statisticPlayer);

            for (int i = 0; i < lenRecords; ++i)
            {
                _gameObject = Instantiate(prefabTextRecord, textRecordTable.transform);
                text = _gameObject.GetComponent<Text>();
                text.text = (i + 1).ToString() + ". " + _statisticPlayer[i].score.ToString() + " : " + _statisticPlayer[i].name.ToString();
            }
        }

    }

    private statisticPlayer[] sortingRecordClass(statisticPlayer[] _statisticPlayer)
    {
        bool flag = false;

        for (int i = 1; i < _statisticPlayer.Length; ++i)
        {
            flag = false;
            for (int j = 0; j < _statisticPlayer.Length - i; ++j)
            {
                if (_statisticPlayer[j].score < _statisticPlayer[j + 1].score)
                {
                    statisticPlayer tmp = _statisticPlayer[j];
                    _statisticPlayer[j] = _statisticPlayer[j + 1];
                    _statisticPlayer[j + 1] = tmp;

                    flag = true;
                }
            }
            if (!flag)
                break;
        }

        return _statisticPlayer;
    }

    private void clearTable()
    {
        foreach (Transform child in textRecordTable.transform) Destroy(child.gameObject);
    }


//------------------Button---------------------------

    public void OnClickRecordButton()
    {
        menuWindow.SetActive(false);
        recordWindow.SetActive(true);

        fullingRecordTable();
    }

    public void OnClickCloseRecordButton()
    {
        menuWindow.SetActive(true);
        recordWindow.SetActive(false);
    }

    public void OnClickClearRecordButton()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();

        clearTable();
        fullingRecordTable();
    }

    public void OnClickPlayButton()
    {
        SceneManager.LoadScene("Game");
    }

    public void OnClickExitButton()
    {
        Application.Quit();
    }
}
