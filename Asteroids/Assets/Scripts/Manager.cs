using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

enum stateSave
{
    lengthRecords,
    stringScoreRecord,
    stringNameRecord
}

public class Manager : MonoBehaviour
{
    [SerializeField] private int maxLife = 3;


    [SerializeField] private Player player;
    [SerializeField] private Text textScore;
    [SerializeField] private Text textLife;

    [SerializeField] private GameObject gameObjectPlayer;
    [SerializeField] private GameObject _gameOverlayWindow;

    [SerializeField] private GameObject _gameOverWindow;
    [SerializeField] private Text deathRecord;

    [SerializeField] private GameObject _saveRecordWindow;
    [SerializeField] private InputField inputName;
    [SerializeField] private Text saveRecord;
    [SerializeField] private Text warningText;

    [SerializeField] private GameObject pauseWindow;


    [SerializeField] private Spawner spawner;


// local date
    private int life;

    private bool pause;

    [HideInInspector] public int score;

    private bool playerImmortal;

 // Events
    private void Start()
    {
        newGame();
    }

    private void Update()
    {
        textScore.text = "Score: " + score.ToString();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                pauseWindow.SetActive(false);
            }
            else
            {
                Time.timeScale = 0;
                pauseWindow.SetActive(true);
            }
        }
    }


// functions
    private void resurrection()
    {
        playerImmortal = false;
        player.resurrection();
    }

    public void PlayerHit(Player player)
    {
        if (--life <= 0)
        {
            PlayerDeath();
        }
        else
        {
            playerImmortal = true;
            Invoke(nameof(resurrection), player.timeImmortal);
        }

        textLife.text = "Life: " + life.ToString();
    }

    private void PlayerDeath()
    {
        gameObjectPlayer.SetActive(false);
        _gameOverlayWindow.SetActive(false);
        _saveRecordWindow.SetActive(true);
        saveRecord.text = "Score: " + score.ToString();
        Time.timeScale = 0;
    }

    public void AsteroidDestroyed(Enemy enemy)
    {
        score += (int)((float)enemy.point * (1 - enemy.size)); // the fewer the more points. Minimum 1 point.
    }

    private void newGame()
    {
        resetManager();
        player.newGame();
        spawner.newGame();

        resurrection();

        gameObjectPlayer.SetActive(true);
        _gameOverlayWindow.SetActive(true);
        _saveRecordWindow.SetActive(false);
        _gameOverWindow.SetActive(false);

        Time.timeScale = 1;
    }

    private void resetManager()
    {
        score = 0;
        life = maxLife;
        playerImmortal = false;
        textLife.text = "Life: " + life.ToString();
        textScore.text = "Score: " + score.ToString();
    }

    private void clearWarning()
    {
        warningText.text = "";
    }

    //------------------Button---------------------------

    public void OnClickNewGameButton()
    {
        newGame();
        gameObjectPlayer.SetActive(true);
        _gameOverlayWindow.SetActive(true);
        _gameOverWindow.SetActive(false);
    }

    public void OnClickMenuButton()
    {
        SceneManager.LoadScene("Menu");
    }

    public void OnClickSaveRecordButton()
    {
        int lenRecords = 0;

        if (inputName.text == "")
        {
            warningText.text = "Invalid name!";
            Invoke(nameof(clearWarning), 2f);
            return;
        }

        if (!PlayerPrefs.HasKey(stateSave.lengthRecords.ToString()))
        {
            PlayerPrefs.SetInt(stateSave.lengthRecords.ToString(), 1);
            PlayerPrefs.SetString(stateSave.stringNameRecord.ToString() + lenRecords.ToString(), inputName.text);
            PlayerPrefs.SetInt(inputName.text, score);
        }
        else if(!PlayerPrefs.HasKey(inputName.text))
        {
            lenRecords = PlayerPrefs.GetInt(stateSave.lengthRecords.ToString());
            PlayerPrefs.SetInt(stateSave.lengthRecords.ToString(), lenRecords + 1);
            PlayerPrefs.SetString(stateSave.stringNameRecord.ToString() + lenRecords.ToString(), inputName.text);
            PlayerPrefs.SetInt(inputName.text, score);
        }
        else
        {
            if (PlayerPrefs.GetInt(inputName.text) < score)
            {
                PlayerPrefs.SetInt(inputName.text, score);
            }
        }
        

        PlayerPrefs.Save();

        deathRecord.text = "Score: " + score.ToString();

        _gameOverWindow.SetActive(true);
        _saveRecordWindow.SetActive(false);
    }

    public void OnClickNoSaveRecordButton()
    {
        _gameOverWindow.SetActive(true);
        _saveRecordWindow.SetActive(false);

        deathRecord.text = "Score: " + score.ToString();
    }

    public void OnClickContinuePauseButton()
    {
        Time.timeScale = 1;
        pauseWindow.SetActive(false);
    }

    public void OnClickExitPauseButton()
    {
        pauseWindow.SetActive(false);

        PlayerDeath();
    }

}
