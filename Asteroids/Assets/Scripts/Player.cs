using UnityEngine;

public class Player : MonoBehaviour
{
// input date
    [SerializeField] private float speedMove = 1f;
    [SerializeField] private float speedTurn = 1f;
    [SerializeField] private float delayShot = 1f;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private EdgeCollider2D _edgeCollider2D;
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private Bullet bulletPrefab;
    [SerializeField] private Transform bulletParentTransform;
    [SerializeField] private Manager manager;

    public float timeImmortal = 1f;


// local date
    private float move;
    private float turn;
    private bool shoot;

    private float oldShot;
    [SerializeField] private bool death;

// Events
    void Start()
    {
        turn = 0;
        move = 0;
        shoot = false;
        death = false;
        oldShot = 0;
    }

    void Update()
    {
        MovementCheckKeyboard();
        RotationCheckKeyboard();

        if (death)
            return;

        ShootCheckKeyboard();
    }

    private void FixedUpdate()
    {
        
        if (move != 0 )
            _rigidbody.AddForce(transform.up * speedMove * move);

        if (turn != 0)
            _rigidbody.AddTorque(turn * speedTurn);


        if (death)
            return;

        float time = Time.time;

        if (shoot && time >= (oldShot + delayShot))
        {
            Shooting();
            oldShot = time;
        }

    }

// functions
    void MovementCheckKeyboard()
    {
        if (Input.GetKey(KeyCode.W)) {
            move = 1f;
        } else if (Input.GetKey(KeyCode.S)) {
            move = -1f;
        } else {
            move = 0;
        }

        return;
    }

    void RotationCheckKeyboard()
    {
        if (Input.GetKey(KeyCode.A)) {
            turn = 1f;
        } else if (Input.GetKey(KeyCode.D)) {
            turn = -1f;
        } else {
            turn = 0;
        }

        return;
    }

    void ShootCheckKeyboard()
    {
        if (Input.GetKey(KeyCode.Space)) {
            shoot = true;
        } else {
            shoot = false;
        }
    }


    void Shooting()
    {
        Bullet bullet = Instantiate(bulletPrefab, transform.position, transform.rotation, bulletParentTransform);
        bullet.Shot(transform.up);
    }

    public void resurrection()
    {
        death = false;
        _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 1f);
    }

    public void newGame()
    {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = 0.0f;

        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
    }



// Collision

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy" && !death)
        {
            _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 0.1f);

            death = true;

            manager.PlayerHit(this);
        }
        else if (collision.gameObject.tag == "Bullet enemy" && !death)
        {
            _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 0.1f);

            death = true;

            manager.PlayerHit(this);
        }
    }
}
