using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] protected Rigidbody2D _rigidbody;
    [SerializeField] protected SpriteRenderer _sprite;
    [SerializeField] protected Manager manager;
    [SerializeField] protected Spawner spawner;

    public float size = 1.0f;
    public float minSize = 0.5f;
    public float maxSize = 1.5f;
    public float movementSpeed = 50.0f;
    public float maxLifetime = 50.0f;
    public int point = 1;

    protected virtual void Awake()
    {
        manager = FindObjectOfType<Manager>();
        spawner = FindObjectOfType<Spawner>();
    }

    protected virtual void Start()
    {
        transform.localScale = Vector3.one * size;
    }

    public virtual void SetTrajectory(Vector2 direction)
    {
        _rigidbody.AddForce(direction * movementSpeed);
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            manager.AsteroidDestroyed(this);

            Destroy(gameObject);
        }
    }

    
}
