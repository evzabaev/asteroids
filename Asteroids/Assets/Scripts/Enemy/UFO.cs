using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFO : Enemy
{
    [SerializeField] private float delayShot;
    [SerializeField] private BulletEnemy bulletPrefab;


    private float oldTime;

    private void FixedUpdate()
    {
        float time = Time.time;

        if (time >= (oldTime + delayShot))
        {
            Shooting();
            oldTime = time;
        }
    }

    void Shooting()
    {
        BulletEnemy bullet = Instantiate(bulletPrefab, transform.position, transform.rotation, spawner.bulletEnemyTransformParent);
        bullet.Shot(Random.insideUnitCircle.normalized);
    }


}
