using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Range(1, 100)]
    [SerializeField] private int startAmountPerSpawn = 1;
    [Range(1, 100)]
    [SerializeField] private int maxAmountPerSpawn = 1;
    [Range(0f, 100f)]
    [SerializeField] private float stepTimeSpawn = 0.5f;
    [Range(0, 10)]
    [SerializeField] private int stepQuantitySpawn = 1;
    [Range(0f, 10f)]
    [SerializeField] private float spawnRate = 1f;
    [Range(0.0f, 45.0f)]
    public float trajectoryVariance = 15.0f;

    [SerializeField]  private float spawnDistance = 12.0f;
    public Transform enemyTransformParent;
    public Transform bulletEnemyTransformParent;

    public Enemy[] enemyPrefab;


    private int quantityPerSpawn;
    private float oldTime;

    private void Start()
    {
        quantityPerSpawn = startAmountPerSpawn;
        oldTime = 0f;
        InvokeRepeating(nameof(spawn), 1f, spawnRate);
    }

    private void Update()
    {
        if (Time.time >= oldTime + stepTimeSpawn && quantityPerSpawn < maxAmountPerSpawn)
        {
            oldTime = Time.time;
            quantityPerSpawn += stepQuantitySpawn;
        }
    }

    private void spawn()
    {
        for (int i = 0; i < quantityPerSpawn; ++i)
        {
            int randomPrefab = Random.Range(0, enemyPrefab.Length);
            Vector2 spawnDirection = Random.insideUnitCircle.normalized;
            Vector2 randomPosition = spawnDirection * spawnDistance;
            Quaternion randomRotation = Quaternion.AngleAxis(Random.Range(-trajectoryVariance, trajectoryVariance), Vector3.forward);
            Enemy enemy = Instantiate(enemyPrefab[randomPrefab], randomPosition, randomRotation, enemyTransformParent);
            enemy.size = Random.Range(enemy.minSize, enemy.maxSize);

            Vector2 trajectory = randomRotation * -spawnDirection;
            enemy.SetTrajectory(trajectory);
        }
    }

    public void newGame()
    {
        quantityPerSpawn = startAmountPerSpawn;
        delEnemy();
    }

    private void delEnemy()
    {
        Enemy[] asteroids = FindObjectsOfType<Enemy>();

        for (int i = 0; i < asteroids.Length; i++)
        {
            Destroy(asteroids[i].gameObject);
        }
    }

}
